import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

import 'dart:io';
import 'dart:async';

final DEBUG = true;

void main() {
  if (DEBUG) {
    HttpOverrides.global = MyHttpOverrides();
  }
  runApp(MinhaApp());
}

class MinhaApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: MinhaHome(),
    );
  }
}

class MinhaHome extends StatefulWidget {
  @override
  _MinhaHome createState() => _MinhaHome();
}

class _MinhaHome extends State<MinhaHome> {
  String data = "";

  void fetch() async {
    try {
      final response = await http.get(Uri.parse('https://192.168.1.4/'));
      if (response.statusCode == 200) {
        setState(() {
          data = response.body;
        });
      }
      else {
        setState(() {
          data = "Jujubinha!";
        });
      }
    }
    catch (error) {
      setState(() {
        data = error.toString();
      });
    }
  }


  @override
  void initState() {
    super.initState();
    fetch();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Text('Hello world! :)\n' + data)
    );
  }
}

// for allowing bad certificates
class MyHttpOverrides extends HttpOverrides {
  @override
  HttpClient createHttpClient(SecurityContext? context) {
    return super.createHttpClient(context)
      ..badCertificateCallback = (X509Certificate cert, String host, int port) => true;
  }
}
